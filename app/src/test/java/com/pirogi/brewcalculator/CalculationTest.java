package com.pirogi.brewcalculator;

import com.pirogi.brewcalculator.data.Calculation;
import com.pirogi.brewcalculator.data.Malt;
import com.pirogi.brewcalculator.data.SavedMalt;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.Delete;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;

import java.lang.reflect.Field;

@Config(constants = BuildConfig.class
        , sdk = 21)
@RunWith(RobolectricGradleTestRunner.class)
public class CalculationTest extends FlowTestCase {

    private static double DELTA = 0.0;

    @Test
    public void testCalculationSetup() {
        Calculation calculation = new Calculation();
        calculation.setName("Test Calculation");
        calculation.setHotWortVolume(18.8);
        calculation.setWortSpecificGravity(0.055);
        calculation.save();

        SavedMalt savedMalt = new SavedMalt();

        Malt malt = new Malt("Pale", 0.78);
        malt.save();

        savedMalt.setMalt(malt);
        savedMalt.setWeight(750);
        savedMalt.setCalculationId(calculation.getId());
        savedMalt.save();

        SavedMalt savedCrystal = new SavedMalt();

        Malt crystal = new Malt("Crystal", 0.74);
        crystal.save();

        savedCrystal.setMalt(crystal);
        savedCrystal.setWeight(100);
        savedCrystal.setCalculationId(calculation.getId());
        savedCrystal.save();

        SavedMalt savedBarley = new SavedMalt();

        Malt barley = new Malt("Flaked Barley", 0.69);
        barley.save();

        savedBarley.setMalt(barley);
        savedBarley.setWeight(55);
        savedBarley.setCalculationId(calculation.getId());
        savedBarley.save();

        assertTrue(calculation.exists());
        assertNotNull(calculation.getId());
        assertEquals(calculation.getName(), "Test Calculation");
        assertEquals(calculation.getHotWortVolume(), 18.8, DELTA);
        assertEquals(calculation.getWortSpecificGravity(), 0.055, DELTA);
        assertEquals(calculation.getActualWortSpecificGravity(), 1.055, DELTA);

        assertTrue(!calculation.getMalts().isEmpty());
        assertEquals(calculation.getMalts().size(), 3);
        assertEquals(calculation.getMalts().get(0).getId(), savedMalt.getId());

        assertEquals(calculation.getWortGravityDegreesPlato(), 13.502369668246445, DELTA);
        assertEquals(calculation.getActualExtract(), 664.0631168428381, DELTA);
        assertEquals(calculation.getPotentialExtract(), 696.95, DELTA);
        assertEquals(calculation.getBrewEfficiency(), 0.9528131384501586, DELTA);
    }

    @Test
    public void testUpdateSavedMaltWeight() {
        Calculation calculation = new Calculation();
        calculation.save();

        SavedMalt savedMalt = new SavedMalt();

        Malt malt = new Malt("Pale", 0.78);
        malt.save();

        savedMalt.setMalt(malt);
        savedMalt.setWeight(750);
        savedMalt.setCalculationId(calculation.getId());
        savedMalt.save();

        assertEquals(calculation.getMalts().get(0).getWeight(), 750.0, DELTA);

        savedMalt.setWeight(800);
        savedMalt.save();
        calculation.refreshMalts();

        assertEquals(calculation.getMalts().get(0).getWeight(), 800.0, DELTA);
    }

    @Test
    public void testGetAllCalculations() {
        Calculation calculation = new Calculation();
        calculation.save();

        Calculation secondCalculation = new Calculation();
        secondCalculation.save();

        assertEquals(Calculation.allCalculations().size(), 2);
    }

    @Test
    public void testCalculationIsSaveable() {
        Calculation calculation = new Calculation();
        calculation.setName("Test Calculation");
        calculation.setHotWortVolume(18.8);
        calculation.setWortSpecificGravity(0.055);
        calculation.save();

        SavedMalt savedMalt = new SavedMalt();

        Malt malt = new Malt("Pale", 0.78);
        malt.save();

        savedMalt.setMalt(malt);
        savedMalt.setWeight(750);
        savedMalt.setCalculationId(calculation.getId());
        savedMalt.save();

        assertTrue(calculation.isSaveable());
    }

    @Test
    public void testCalculationIsNotSaveableWithoutAllInputs() {
        Calculation calculation = new Calculation();
        calculation.setName("Test Calculation");
        calculation.setWortSpecificGravity(0.055);
        calculation.save();

        SavedMalt savedMalt = new SavedMalt();

        Malt malt = new Malt("Pale", 0.78);
        malt.save();

        savedMalt.setMalt(malt);
        savedMalt.setWeight(750);
        savedMalt.setCalculationId(calculation.getId());
        savedMalt.save();
        calculation.refreshMalts();

        assertTrue(!calculation.isSaveable());
    }

    @Test
    public void testCalculationIsNotSaveableWithoutSavedMalts() {
        Calculation calculation = new Calculation();
        calculation.setName("Test Calculation");
        calculation.setHotWortVolume(18.8);
        calculation.setWortSpecificGravity(0.055);
        calculation.save();

        assertTrue(!calculation.isSaveable());
    }

    @After
    public void tearDown() throws Exception {
        Field field = FlowManager.class.getDeclaredField("mDatabaseHolder");
        field.setAccessible(true);
        field.set(null, null);
    }

}
