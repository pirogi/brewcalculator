package com.pirogi.brewcalculator;

import com.pirogi.brewcalculator.data.Calculation;
import com.pirogi.brewcalculator.data.Malt;
import com.pirogi.brewcalculator.data.SavedMalt;
import com.raizlabs.android.dbflow.sql.language.Delete;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;

@Config(constants = BuildConfig.class
        , sdk = 21)
@RunWith(RobolectricGradleTestRunner.class)
public class SavedMaltTest extends FlowTestCase {

    private static double DELTA = 0.0;

    @Test
    public void testSavedMaltSetup() {
        Delete.tables(Calculation.class, Malt.class, SavedMalt.class);

        SavedMalt savedMalt = new SavedMalt();

        Malt malt = new Malt();
        malt.setName("Pale");
        malt.setRatio(0.78);
        malt.save();

        savedMalt.setMalt(malt);
        savedMalt.setWeight(750);
        savedMalt.setCalculationId(1);
        savedMalt.save();

        assertTrue(savedMalt.exists());
        assertNotNull(savedMalt.getId());
        assertTrue(savedMalt.getMalt().exists());
        assertNotNull(savedMalt.getMalt().getId());
        assertEquals(savedMalt.getMalt().getName(), "Pale");
        assertEquals(savedMalt.getMalt().getRatio(), 0.78, DELTA);
        assertEquals(savedMalt.getCalculationId(), 1, DELTA);
        assertEquals(savedMalt.getWeight(), 750, DELTA);
    }

}
