package com.pirogi.brewcalculator;

import android.app.Activity;

import com.pirogi.brewcalculator.data.Calculation;
import com.pirogi.brewcalculator.data.Malt;
import com.pirogi.brewcalculator.data.SavedMalt;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.Delete;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.annotation.Config;

import java.lang.reflect.Field;
import java.util.List;

@Config(constants = BuildConfig.class
        , sdk = 21)
@RunWith(RobolectricGradleTestRunner.class)
public class MaltTest extends FlowTestCase {

    private static double DELTA = 0.0;

    @Test
    public void testMaltSetup() {
        Malt malt = new Malt();
        malt.setName("Pale");
        malt.setRatio(0.78);
        malt.save();

        assertTrue(malt.exists());
        assertNotNull(malt.getId());
        assertEquals(malt.getName(), "Pale");
        assertEquals(malt.getRatio(), 0.78, DELTA);
        assertEquals(malt.getDetailedName(), "Pale (0.78)");
        assertEquals(malt.getFormattedRatio(),"0.78");
    }

    @Test
    public void testGetAllMalts() {
        Malt crystal = new Malt("Crystal", 0.78);
        crystal.save();

        Malt pale = new Malt("Pale", 0.74);
        pale.save();

        List<Malt> malts = Malt.allMalts();

        assertTrue(!malts.isEmpty());
        assertEquals(malts.size(), 2.0, DELTA);
    }

    @Test
    public void testGetAllMaltNames() {
        Malt crystal = new Malt("Crystal", 0.78);
        crystal.save();

        Malt pale = new Malt("Pale", 0.74);
        pale.save();

        List<String> maltNames = Malt.allMaltNames((Activity)getContext());

        assertTrue(!maltNames.isEmpty());
        assertEquals(maltNames.size(), 2.0, DELTA);
        assertEquals(maltNames.get(0), "Crystal (0.78)");
    }

    @Test
    public void testMaltSetupWithVariables() {
        Malt malt = new Malt("Crystal", 0.78);
        malt.save();

        assertTrue(malt.exists());
        assertNotNull(malt.getId());
        assertEquals(malt.getName(), "Crystal");
        assertEquals(malt.getRatio(), 0.78, DELTA);
        assertEquals(malt.getDetailedName(), "Crystal (0.78)");
        assertEquals(malt.getFormattedRatio(),"0.78");
    }

    @After
    public void tearDown() throws Exception {
        Field field = FlowManager.class.getDeclaredField("mDatabaseHolder");
        field.setAccessible(true);
        field.set(null, null);
    }

}
