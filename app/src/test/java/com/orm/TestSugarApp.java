package com.orm;

import android.app.Application;
import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.TestLifecycleApplication;
import org.robolectric.annotation.Config;

import java.lang.reflect.Method;

/**
 * Created by Subathra Thanabalan on 10/21/15.
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest="./src/main/AndroidManifest.xml", sdk=21)
public class TestSugarApp extends Application
        implements TestLifecycleApplication {

    @Test
    public void startEverTestSugarAppAsFirst() {

    }

    @Override
    public void beforeTest(Method method) {
        Log.v("test", "beforeTest");
    }

    @Override
    public void prepareTest(Object test) {
        Log.v("test", "prepareTest");
    }

    @Override
    public void afterTest(Method method) {
        Log.v("test", "afterTest");
    }
}