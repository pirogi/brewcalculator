package com.pirogi.brewcalculator.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pirogi.brewcalculator.BrewApplication;
import com.pirogi.brewcalculator.R;
import com.pirogi.brewcalculator.adapter.MaltAdapter;
import com.pirogi.brewcalculator.data.Malt;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Subathra Thanabalan on 10/17/15.
 */
public class MaltFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_malt, container, false);

        FloatingActionButton addFAB = (FloatingActionButton) view.findViewById(R.id.addMalt);
        addFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presentAddMaltView();
            }
        });

        mRecyclerView = (RecyclerView) view.findViewById(R.id.maltList);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        List<Malt> malts = getMalts();

        mAdapter = new MaltAdapter(malts);
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    private void presentAddMaltView() {
        Log.d(MaltFragment.class.getName(), "Add Malt");
    }

    private List<Malt> getMalts() {
        return Malt.allMalts();
    }
}
