package com.pirogi.brewcalculator.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.pirogi.brewcalculator.R;
import com.pirogi.brewcalculator.adapter.CalculationMaltAdapter;
import com.pirogi.brewcalculator.data.Malt;
import com.pirogi.brewcalculator.data.SavedMalt;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Subathra Thanabalan on 10/17/15.
 */
public class CalculationMaltFragment extends Fragment implements Button.OnClickListener {

    private ListView listView;
    private CalculationMaltAdapter adapter;
    private List<Malt> maltList;
    MaltInputListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calculation_malt, container, false);

        listView = (ListView) view.findViewById(R.id.addMaltList);

        listener = (MaltInputListener) getActivity();
        adapter = new CalculationMaltAdapter(getActivity(), listener);
        listView.setAdapter(adapter);
        listView.setDivider(null);
        
        maltList = getMalts();

        Button addMalt = (Button) view.findViewById(R.id.addMaltButton);
        addMalt.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.list_select_malt, getMaltNames());
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Malt m = getMalt(which);
                SavedMalt sm = new SavedMalt(m);
                sm.save();
                CalculationMaltFragment.this.adapter.addMalt(sm);
                listener.onMaltAdded(sm);
                CalculationMaltFragment.this.adapter.notifyDataSetChanged();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private List<String> getMaltNames() {
        ArrayList<String> maltNames = (ArrayList<String>) Malt.allMaltNames(getActivity());
//        maltNames.add(0, getString(R.string.prompt_add_custom_malt));
        return maltNames;
    }

    private Malt getMalt(int position) {
        return maltList.get(position);
    }

    private List<Malt> getMalts() {
        return Malt.allMalts();
    }

    public interface MaltInputListener {
        public void onMaltAdded(SavedMalt malt);
        public void onMaltWeightChanged(SavedMalt malt);
    }
}
