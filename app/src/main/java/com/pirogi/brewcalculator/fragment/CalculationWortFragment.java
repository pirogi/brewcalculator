package com.pirogi.brewcalculator.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.pirogi.brewcalculator.R;
import com.pirogi.brewcalculator.data.Calculation;

/**
 * Created by Subathra Thanabalan on 10/17/15.
 */
public class CalculationWortFragment extends Fragment {

    WortInputListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calculation_wort, container, false);

        listener = (WortInputListener) getActivity();
        setupListeners(view);

        return view;
    }

    private void setupListeners(View view) {

        EditText wortET = (EditText) view.findViewById(R.id.valueWortVolume);
        wortET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                Double volume = new Double(0);
                try {
                    if (s.length() > 0) {
                        volume = Double.parseDouble(s.toString());
                    }
                } catch (Exception e) {
                    Log.e(CalculationWortFragment.class.getName(), e.getMessage());
                }

                listener.onHotWortVolumeChanged(volume);
            }
        });

        EditText specificET = (EditText) view.findViewById(R.id.valueSpecificGravity);
        final EditText degreesET = (EditText) view.findViewById(R.id.valueGravityDegrees);
        specificET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                Double gravity = new Double(0);
                try {
                    if (s.length() > 0) {
                        gravity = Double.parseDouble(s.toString());
                    }
                } catch (Exception e) {
                    Log.e(CalculationWortFragment.class.getName(), e.getMessage());
                }

                gravity = gravity * 0.001;
                listener.onSpecificGravityChanged(gravity);
                String degrees = String.format("%s",Calculation.getGravityDegreesPlato(Calculation.getActualWortSpecificGravity(gravity)));
                degreesET.setText(degrees);
            }
        });
    }

    public interface WortInputListener {
        public void onHotWortVolumeChanged(Double volume);
        public void onSpecificGravityChanged(Double gravity);
    }
}
