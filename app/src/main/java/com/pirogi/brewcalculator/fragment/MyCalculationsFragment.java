package com.pirogi.brewcalculator.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pirogi.brewcalculator.R;
import com.pirogi.brewcalculator.adapter.CalculationAdapter;
import com.pirogi.brewcalculator.data.Calculation;

import java.util.List;

/**
 * Created by Subathra Thanabalan on 10/17/15.
 */
public class MyCalculationsFragment extends Fragment {

    OnAddCalculationSelectedListener mListener;
    private RecyclerView mRecyclerView;
    private CalculationAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_calculations, container, false);

        FloatingActionButton addFAB = (FloatingActionButton) view.findViewById(R.id.fab_add_calculation);
        addFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presentAddCalculationView();
            }
        });

        mRecyclerView = (RecyclerView) view.findViewById(R.id.calculationList);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        List<Calculation> calculations = getCalculations();

        mAdapter = new CalculationAdapter(calculations);
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.updateDataset(getCalculations());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mListener = (OnAddCalculationSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement OnAddCalculationSelectedListener");
        }
    }

    private void presentAddCalculationView() {
        mListener.onAddCalculationSelected();
    }

    private List<Calculation> getCalculations() {
        return Calculation.allCalculations();
    }

    public void setOnAddCalculationSelectedListener(OnAddCalculationSelectedListener listener) {
        mListener = listener;
    }

    public interface OnAddCalculationSelectedListener {
        public void onAddCalculationSelected();
    }

}
