package com.pirogi.brewcalculator.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.pirogi.brewcalculator.R;
import com.pirogi.brewcalculator.adapter.CalculationViewsAdapter;
import com.pirogi.brewcalculator.data.Calculation;
import com.pirogi.brewcalculator.data.SavedMalt;
import com.pirogi.brewcalculator.fragment.CalculationMaltFragment;
import com.pirogi.brewcalculator.fragment.CalculationWortFragment;

import java.util.Date;

/**
 * Created by Subathra Thanabalan on 10/17/15.
 */
public class CalculationActivity extends AppCompatActivity
        implements CalculationMaltFragment.MaltInputListener, CalculationWortFragment.WortInputListener {

    private Calculation calculation;
    private FloatingActionButton saveButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculation);

        calculation = new Calculation();
        calculation.setName("Calculation " + new Date().toString());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setCustomView(R.layout.view_actionbar);
        EditText nameField = (EditText) getSupportActionBar().getCustomView().findViewById(R.id.nameField);
        nameField.setText(calculation.getName());
        nameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                calculation.setName(s.toString());
            }
        });
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM |
                ActionBar.DISPLAY_SHOW_HOME);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        setupViewPager(pager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(pager);

        saveButton = (FloatingActionButton) findViewById(R.id.fab_save_calculation);
//        saveButton.setVisibility(calculation.isSaveable() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        CalculationViewsAdapter adapter = new CalculationViewsAdapter(getFragmentManager());

        adapter.addFragment(new CalculationWortFragment(), getString(R.string.title_wort));
        adapter.addFragment(new CalculationMaltFragment(), getString(R.string.title_malt));

        viewPager.setAdapter(adapter);
    }

    public void updateView() {
        EditText potential = (EditText) findViewById(R.id.valuePotential);
        potential.setText(calculation.getFormattedPotentialExtract());

        EditText actual = (EditText) findViewById(R.id.valueActual);
        actual.setText(calculation.getFormattedActualExtract());

        TextView efficiency = (TextView) findViewById(R.id.valueEfficiency);
        efficiency.setText(calculation.getFormattedBrewEfficiency());

        if (calculation.isSaveable()) {
            saveButton.show();
        } else {
            saveButton.hide();
        }
    }

    @Override
    public void onMaltAdded(SavedMalt malt) {
        Log.i(CalculationActivity.class.getName(), "onMaltAdded");

        malt.setCalculationId(calculation.getId());
        malt.save();
    }

    @Override
    public void onMaltWeightChanged(SavedMalt malt) {
        Log.i(CalculationActivity.class.getName(), "onMaltWeightChanged "+malt.getMalt().getDetailedName());
        Log.i(CalculationActivity.class.getName(), "weight " + malt.getWeight());

        calculation.refreshMalts();

        updateView();
    }

    @Override
    public void onHotWortVolumeChanged(Double volume) {
        calculation.setHotWortVolume(volume);
        calculation.save();
    }

    @Override
    public void onSpecificGravityChanged(Double gravity) {
        calculation.setWortSpecificGravity(gravity);
        calculation.save();
    }
}
