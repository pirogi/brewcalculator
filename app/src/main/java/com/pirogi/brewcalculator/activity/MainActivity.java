package com.pirogi.brewcalculator.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.pirogi.brewcalculator.R;
import com.pirogi.brewcalculator.data.Malt;
import com.pirogi.brewcalculator.fragment.MaltFragment;
import com.pirogi.brewcalculator.fragment.MyCalculationsFragment;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, MyCalculationsFragment.OnAddCalculationSelectedListener {

    private static final long DRAWER_CLOSE_DELAY_MS = 250;
    private static final String NAV_ITEM_ID = "navItemId";
    private static final String FIRST_RUN_KEY = "firstrun";

    private final Handler mDrawerActionHandler = new Handler();
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private MenuItem mNavItem;

    SharedPreferences prefs = null;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fabric.with(this, new Crashlytics());

        prefs = getSharedPreferences("com.pirogi.brewcalculator", MODE_PRIVATE);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // listen for navigation events
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(this);

        // load saved navigation state if present
        if (null == savedInstanceState) {
            mNavItem = navigationView.getMenu().findItem(R.id.drawer_item_my_calculations);;
        } else {
            mNavItem = navigationView.getMenu().findItem(savedInstanceState.getInt(NAV_ITEM_ID));
        }

        // select the correct nav menu item
        navigationView.getMenu().findItem(mNavItem.getItemId()).setChecked(true);

        // set up the hamburger icon to open and close the drawer
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open,
                R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        navigate(mNavItem);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (prefs.getBoolean(FIRST_RUN_KEY, true)) {
            generateDefaultMalts();
            prefs.edit().putBoolean(FIRST_RUN_KEY, false).commit();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void navigate(final MenuItem item) {
        Fragment fragment = new Fragment();

        if (item.getItemId() == R.id.drawer_item_my_calculations) {
            fragment = new MyCalculationsFragment();
            ((MyCalculationsFragment) fragment).setOnAddCalculationSelectedListener(this);
        } else if (item.getItemId() == R.id.drawer_item_malt) {
            fragment = new MaltFragment();
        }

        addFragment(fragment);
        getSupportActionBar().setTitle(item.getTitle());
    }

    @Override
    public boolean onNavigationItemSelected(final MenuItem menuItem) {
        // update highlighted item in the navigation menu
        menuItem.setChecked(true);
        mNavItem = menuItem;

        // allow some time after closing the drawer before performing real navigation
        // so the user can see what is happening
        mDrawerLayout.closeDrawer(GravityCompat.START);
        mDrawerActionHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navigate(menuItem);
            }
        }, DRAWER_CLOSE_DELAY_MS);
        return true;
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(NAV_ITEM_ID, mNavItem.getItemId());
    }

    @Override
    public void onAddCalculationSelected() {
        Intent intent = new Intent(MainActivity.this, CalculationActivity.class);
        MainActivity.this.startActivity(intent);
    }

    private void addFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content, fragment)
                .commit();
    }

    private void generateDefaultMalts() {
        Malt pale = new Malt("Pale", 0.78);
        pale.save();

        Malt crystal = new Malt("Crystal", 0.74);
        crystal.save();

        Malt barley = new Malt("Flaked Barley", 0.69);
        barley.save();
    }

}