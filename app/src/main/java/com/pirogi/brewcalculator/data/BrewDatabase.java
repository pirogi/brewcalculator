package com.pirogi.brewcalculator.data;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = BrewDatabase.NAME, version = BrewDatabase.VERSION)
public class BrewDatabase {

    public static final String NAME = "Brewsly";

    public static final int VERSION = 1;

}
