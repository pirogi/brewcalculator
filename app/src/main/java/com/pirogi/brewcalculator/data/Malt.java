package com.pirogi.brewcalculator.data;

import android.app.Activity;
import android.content.Context;
import com.pirogi.brewcalculator.BrewApplication;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Table(databaseName = BrewDatabase.NAME)
public class Malt extends BaseModel {

    @Column
    @PrimaryKey(autoincrement = true)
    long id;

    @Column
    String name;

    @Column
    double ratio;

    public Malt() {
    }

    public Malt(String name, double ratio) {
        this.name = name;
        this.ratio = ratio;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    public String getDetailedName() {
        return name+" ("+ratio+")";
    }

    public String getFormattedRatio() {
        return String.format("%s",ratio);
    }

    public static List<String> allMaltNames(Activity activity) {
        List<Malt> malts = allMalts();
        List<String > names = new ArrayList<String>();
        for (Malt m : malts) {
            names.add(m.getDetailedName());
        }
        return names;
    }

    public static List<Malt> allMalts() {
        List<Malt> malts = new ArrayList<Malt>();
        malts = new Select().from(Malt.class).queryList();
        return malts;
    }

}
