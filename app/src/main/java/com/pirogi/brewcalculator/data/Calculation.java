package com.pirogi.brewcalculator.data;

import android.util.Log;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ModelContainer;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.builder.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@ModelContainer
@Table(databaseName = BrewDatabase.NAME)
public class Calculation extends BaseModel {

    @Column
    @PrimaryKey(autoincrement = true)
    long id;

    @Column
    private String name;

    @Column
    Date dateCreated;

    @Column
    double potentialExtract;

    @Column
    double actualExtract;

    @Column
    double brewEfficiency;

    // Inputs

    @Column
    double hotWortVolume;

    @Column
    double wortSpecificGravity;

    @Column
    double wortGravityDegreesPlato;

    // Malts
    List<SavedMalt> malts;

    boolean saveable;

    // Constants
    private static final int GRAVITY_DEGREES_CONSTANT = 259;

    public Calculation() {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPotentialExtract() {
        calculatePotentialExtract();
        return potentialExtract;
    }

    public String getFormattedPotentialExtract() {
        return String.format("%s", getPotentialExtract());
    }

    public double getActualExtract() {
        calculateActualExtract();
        return actualExtract;
    }

    public String getFormattedActualExtract() {
        return String.format("%s", getActualExtract());
    }

    public double getBrewEfficiency() {
        calculateBrewEfficiency();
        return brewEfficiency;
    }

    public String getFormattedBrewEfficiency() {
        NumberFormat defaultFormat = NumberFormat.getPercentInstance();
        defaultFormat.setMinimumFractionDigits(2);
        defaultFormat.setMaximumFractionDigits(2);
        return defaultFormat.format(getBrewEfficiency());
    }

    public double getHotWortVolume() {
        return hotWortVolume;
    }

    public void setHotWortVolume(double hotWortVolume) {
        this.hotWortVolume = hotWortVolume;
    }

    public double getWortSpecificGravity() {
        return wortSpecificGravity;
    }

    public double getActualWortSpecificGravity() {
        return Calculation.getActualWortSpecificGravity(this.getWortSpecificGravity());
    }

    public static double getActualWortSpecificGravity(double wortSpecificGravity) {
        return 1.0 + wortSpecificGravity;
    }

    public void setWortSpecificGravity(double wortSpecificGravity) {
        this.wortSpecificGravity = wortSpecificGravity;
        calculateGravityDegreesPlato();
    }

    public double getWortGravityDegreesPlato() {
        return wortGravityDegreesPlato;
    }

//    @OneToMany(methods = {OneToMany.Method.SAVE, OneToMany.Method.DELETE}, variableName = "malts")
//    public List<SavedMalt> getMalts() {
//        if (malts == null || malts.isEmpty()) {
//            malts = new Select().from(SavedMalt.class)
//                    .where(Condition.column(SavedMalt$Table.CALCULATIONMODELCONTAINER_CALCULATION_ID).is(getId()))
//                    .queryList();
//        }
//        return malts;
//    }

    public List<SavedMalt> getMalts() {
        if (malts == null || malts.isEmpty()) {
            malts = new Select().from(SavedMalt.class)
                    .where(Condition.column(SavedMalt$Table.CALCULATION_ID).is(id))
                    .queryList();
        }
        return malts;
    }

    public void setMalts(List<SavedMalt> malts) {
        this.malts = malts;
    }

    public void addMalt(SavedMalt malt) {
        this.malts.add(malt);
    }

    public void refreshMalts() {
        malts = null;
        getMalts();
    }

    // Calculations

    private void calculatePotentialExtract() {
        double potential = 0;

        Iterator it = getMalts().iterator();
        while (it.hasNext()) {
            SavedMalt m = (SavedMalt) it.next();
            potential = potential + (m.getWeight() * m.getMalt().getRatio());
        }

        potentialExtract = potential;
    }

    private void calculateActualExtract() {
        actualExtract =
                (((GRAVITY_DEGREES_CONSTANT+getWortGravityDegreesPlato())*getWortGravityDegreesPlato())/100)*(getHotWortVolume()*0.96);
    }

    public static double getGravityDegreesPlato(double wortSpecificGravity) {
        return GRAVITY_DEGREES_CONSTANT - (GRAVITY_DEGREES_CONSTANT/wortSpecificGravity);
    }

    private void calculateGravityDegreesPlato() {
        wortGravityDegreesPlato = getGravityDegreesPlato(getActualWortSpecificGravity());
    }

    private void calculateBrewEfficiency() {
        brewEfficiency = getActualExtract() / getPotentialExtract();
    }

    public static List<Calculation> allCalculations() {
        List<Calculation> calculations = new Select().from(Calculation.class).queryList();
        return calculations;
    }

    public boolean isSaveable() {
        boolean hasAllInputs = ((getHotWortVolume() > 0.0) && (getWortSpecificGravity() > 0.0));
        boolean hasMalts = !getMalts().isEmpty();
        return (hasAllInputs && hasMalts);
    }
}
