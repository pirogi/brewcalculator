package com.pirogi.brewcalculator.data;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.ForeignKeyReference;
import com.raizlabs.android.dbflow.annotation.ModelContainer;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.raizlabs.android.dbflow.structure.container.ForeignKeyContainer;

@Table(databaseName = BrewDatabase.NAME)
public class SavedMalt extends BaseModel {

    public static final String MALT_ID_FIELD_NAME = "malt_id";
    public static final String CALCULATION_ID_FIELD_NAME = "calculation_id";

    @Column
    @PrimaryKey(autoincrement = true)
    long id;

    @Column
    @ForeignKey(references = {
            @ForeignKeyReference(columnName = MALT_ID_FIELD_NAME, columnType = Long.class, foreignColumnName = "id")},
            saveForeignKeyModel = false)
    Malt malt;

    @Column
    double weight;

//    @Column
//    @ForeignKey(references = {
//            @ForeignKeyReference(columnName = CALCULATION_ID_FIELD_NAME, columnType = Long.class, foreignColumnName = "id")},
//            saveForeignKeyModel = false)
//    ForeignKeyContainer<Calculation> calculationModelContainer;

    @Column
    long calculation_id;

    public SavedMalt() {
        this.weight = 0.0;
    }

    public SavedMalt(Malt malt) {
        this.malt = malt;
        this.weight = 0.0;
    }

    public long getId() {
        return id;
    }

    public Malt getMalt() {
        return malt;
    }

    public void setMalt(Malt malt) {
        this.malt = malt;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public String getFormattedWeight() {
        return String.format("%s", weight);
    }

    public String getName() {
        return malt.getName();
    }

    public long getCalculationId() {
        return calculation_id;
    }

    public void setCalculationId(long calculation_id) {
        this.calculation_id = calculation_id;
    }

//    public void associateCalculation(Calculation calculation) {
//        calculationModelContainer = new ForeignKeyContainer<>(Calculation.class);
//        calculationModelContainer.setModel(calculation);
//    }

}
