package com.pirogi.brewcalculator.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pirogi.brewcalculator.R;
import com.pirogi.brewcalculator.data.Calculation;

import java.util.List;

/**
 * Created by Subathra Thanabalan on 10/24/15.
 */
public class CalculationAdapter extends RecyclerView.Adapter<CalculationAdapter.ViewHolder> {

    private List<Calculation> mDataset;

    public CalculationAdapter(List<Calculation> calculations) {
        mDataset = calculations;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_calculation, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Calculation c = mDataset.get(position);
        TextView tv = (TextView) holder.mLayout.findViewById(R.id.name);
        tv.setText(c.getName());
        TextView effTV = (TextView) holder.mLayout.findViewById(R.id.valueEfficiency);
        effTV.setText(c.getFormattedBrewEfficiency());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void updateDataset(List<Calculation> data) {
        mDataset = data;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout mLayout;
        public ViewHolder(LinearLayout v) {
            super(v);
            mLayout = v;
        }
    }
}
