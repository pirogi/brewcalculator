package com.pirogi.brewcalculator.adapter;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.pirogi.brewcalculator.R;
import com.pirogi.brewcalculator.data.SavedMalt;
import com.pirogi.brewcalculator.fragment.CalculationMaltFragment;

import java.util.ArrayList;

/**
 * Created by Subathra Thanabalan on 10/18/15.
 */
public class CalculationMaltAdapter extends BaseAdapter {

    private ArrayList<SavedMalt> mDataset;
    private static CalculationMaltFragment.MaltInputListener mListener;
    private LayoutInflater inflater;
    Context context;

    public CalculationMaltAdapter(Context context, CalculationMaltFragment.MaltInputListener listener) {
        mDataset = new ArrayList<SavedMalt>();
        mListener = listener;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return mDataset.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataset.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mDataset.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final CalculationMaltAdapter.ViewHolder vh;
        convertView = inflater.inflate(R.layout.list_calculation_malt, parent, false);
        vh = new CalculationMaltAdapter.ViewHolder((LinearLayout) convertView);
        convertView.setTag(vh);

        final SavedMalt malt = (SavedMalt) getItem(position);
        vh.setMalt(malt);

        TextInputLayout til = (TextInputLayout) vh.mLayout.findViewById(R.id.maltWeightInput);
        til.setHint(malt.getName() + " (lbs)");

        EditText et = (EditText) convertView.findViewById(R.id.maltWeightValue);
        et.setText(""+malt.getWeight());

        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                Double weight = new Double(0);
                try {
                    if (s.length() > 0) {
                        weight = Double.parseDouble(s.toString());
                        malt.setWeight(weight);
                        malt.save();
                    }
                } catch (Exception e) {
                    Log.e(CalculationMaltAdapter.class.getName(), e.getMessage());
                }

                mListener.onMaltWeightChanged(vh.getMalt());
            }
        });

        return convertView;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout mLayout;
        private SavedMalt mMalt;

        public ViewHolder(LinearLayout layout) {
            super(layout);
            mLayout = layout;
        }

        public SavedMalt getMalt() {
            return mMalt;
        }

        public void setMalt(SavedMalt malt) {
            mMalt = malt;
        }
    }

    public void addMalt(SavedMalt malt) {
        mDataset.add(malt);
    }
}
