package com.pirogi.brewcalculator.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pirogi.brewcalculator.R;
import com.pirogi.brewcalculator.data.Malt;

import java.util.List;

/**
 * Created by Subathra Thanabalan on 10/17/15.
 */
public class MaltAdapter extends RecyclerView.Adapter<MaltAdapter.ViewHolder> {

    private List<Malt> mDataset;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout mLayout;
        public ViewHolder(LinearLayout v) {
            super(v);
            mLayout = v;
        }
    }

    public MaltAdapter(List<Malt> dataset) {
        mDataset = dataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.list_malt, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TextView tv = (TextView) holder.mLayout.findViewById(R.id.name);
        tv.setText(mDataset.get(position).getName());
        TextView ratioTV = (TextView) holder.mLayout.findViewById(R.id.ratio);
        ratioTV.setText(mDataset.get(position).getFormattedRatio());
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
