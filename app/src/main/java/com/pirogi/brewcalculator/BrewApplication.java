package com.pirogi.brewcalculator;

import android.app.Application;

import com.raizlabs.android.dbflow.config.FlowManager;

/**
 * Created by Subathra Thanabalan on 10/26/15.
 */
public class BrewApplication extends Application {

    private static BrewApplication singleton;

    public BrewApplication() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        FlowManager.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
